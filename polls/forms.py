from django import forms
from polls.classes import PasoPorReferencia as ppr, Arbol as a

def esEntero(num):
    num2 = num - int(num)
    return num2 < 1E-10

def mediana(L0):
    N = len(L0)
    if N % 2 == 0:
        pos1 = N // 2
        pos2 = pos1 - 1
        result = (L0[pos1] + L0[pos2]) / 2.0
        if esEntero(result):
            return "%.0f" % result
        else:
            return result.__str__()
    else:
        pos = N // 2
        return L0[pos].__str__()

class EntryForm(forms.Form):
    str_entry = forms.CharField(label='Entrada.', required=True)

    def medianResult(self):
        entry = self.data['entry']
        output = ''
        lines = entry.split("\n")
        N = int(lines[0])
        L = []
        for i in range(1, N+1):
            line = lines[i]
            words = line.split(" ")
            action = words[0]
            element = int(words[1])
            if action == 'a':
                L.append(element)
                L.sort()
                output += mediana(L)
            elif action == 'r':
                try:
                    j = L.index(element)
                    L.pop(j)
                    output += mediana(L)
                except:
                    output += "Wrong!"
            else:
                output += "Wrong!"
            output += "\n"
        return output

    def treesResult(self):
        entry = self.data['entry']
        output = ''
        lines = entry.split("\n")
        N = int(lines[0])
        L_nodos_ids = range(1, N+1)
        L_colores =  lines[1].split(" ")
        for i in range(N):
            L_colores[i] = int(L_colores[i])
        linea = lines[2]
        partes = linea.split(" ")
        padre_id = int(partes[0])
        hijo_id = int(partes[1])
        arbol0 = a.Arbol(padre_id, L_colores[padre_id - 1])
        hijo_color = L_colores[hijo_id - 1]
        band_sin_asignar = ppr.PasoPorReferencia(True)
        arbol0.asignarHijo(padre_id, hijo_id, hijo_color, band_sin_asignar)
        for j in range(3, N+1):
            linea = lines[j]
            partes = linea.split(" ")
            padre_id = int(partes[0])
            hijo_id  = int(partes[1])
            hijo_color = L_colores[hijo_id - 1]
            band_sin_asignar = ppr.PasoPorReferencia(True)
            arbol0.asignarHijo(padre_id, hijo_id, hijo_color, band_sin_asignar)
        for nodo1 in L_nodos_ids:
            acum = 0
            for nodo2 in L_nodos_ids:
                acum += arbol0.d(nodo1, nodo2)
            output += acum.__str__()+"\n"
        return output
