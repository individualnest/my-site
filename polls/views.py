from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.template import loader
from .forms import EntryForm

def index(request):
    template = loader.get_template('polls/index.html')
    context = {}
    return HttpResponse(template.render(context, request))

def mediana(request):
    context = {'entrada':'', 'resultado': ''}
    if request.method == 'POST':
        form = EntryForm(request.POST)
        context = {'entrada': form.data['entry'], 'resultado': form.medianResult()}
    else:
        form = EntryForm()
    template = loader.get_template('polls/mediana.html')
    return HttpResponse(template.render(context, request), {'form': form})

def arboles(request):
    context = {'entrada':'', 'resultado': ''}
    if request.method == 'POST':
        form = EntryForm(request.POST)
        context = {'entrada': form.data['entry'], 'resultado': form.treesResult()}
    else:
        form = EntryForm()
    template = loader.get_template('polls/arboles.html')
    return HttpResponse(template.render(context, request), {'form': form})