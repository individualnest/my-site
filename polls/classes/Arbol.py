from polls.classes import PasoPorReferencia as ppr

class Arbol:
	def __init__(self, id0, color0, hijo_izq0=None, hijo_der0=None):
		self.__id = id0
		self.__color = color0
		self._hijo_izq = hijo_izq0
		self._hijo_der = hijo_der0
	def getId(self):
		return self.__id
	def getColor(self):
		return self.__color
	def getNodo(self):
		return Arbol(self.__id, self.__color)
	def getHijoIzq(self):
		return self._hijo_izq
	def getHijoDer(self):
		return self._hijo_der
	def setId(self, id0):
		self.__id = id0
	def setColor(self, color0):
		self.__color = color0
	def setHijoIzq(self, hijo_izq0):
		self._hijo_izq = hijo_izq0
	def setHijoDer(self, hijo_der0):
		self._hijo_der = hijo_der0
	def existeHijoIzq(self):
		return (self._hijo_izq != None)
	def existeHijoDer(self):
		return (self._hijo_der != None)
	def asignarHijo(self, padre_id, hijo_id, hijo_color, band_sin_asignar):
		if self.getId()==padre_id:
			if not self.existeHijoIzq():
				self.setHijoIzq(Arbol(hijo_id, hijo_color))
			else:
				self.setHijoDer(Arbol(hijo_id, hijo_color))
			band_sin_asignar.setValue(False)
		else:
			if self.existeHijoIzq():
				self.getHijoIzq().asignarHijo(padre_id, hijo_id, hijo_color, band_sin_asignar)
			if band_sin_asignar.getValue() and self.existeHijoDer():
				self.getHijoDer().asignarHijo(padre_id, hijo_id, hijo_color, band_sin_asignar)
	def caminoReverso(self, id0, band_encontro):
		pila = []
		if self.existeHijoIzq() and (not band_encontro.getValue()):
			pila = self.getHijoIzq().caminoReverso(id0, band_encontro)
		if self.existeHijoDer() and (not band_encontro.getValue()):
			pila = self.getHijoDer().caminoReverso(id0, band_encontro)
		if self.getId()==id0 and (not band_encontro.getValue()):
			band_encontro.setValue(True)
			return [self.getNodo()]
		elif band_encontro.getValue():
			pila.append(self.getNodo())
		return pila
	def d(self, id1, id2):
		band_encontro = ppr.PasoPorReferencia(False)
		camino1 = self.caminoReverso(id1, band_encontro)
		band_encontro = ppr.PasoPorReferencia(False)
		camino2 = self.caminoReverso(id2, band_encontro)
		i = 0
		N = len(camino1)
		L = []
		punto = None
		while i<N and punto==None:
			L.append(camino1[i].getColor())
			if camino1[i] in camino2:
				punto = camino1[i].getId()
			i += 1
		i = 0
		N = len(camino2)
		continua = True
		while i<N and continua:
			if punto != camino2[i].getId():
				L.append(camino2[i].getColor())
			else:
				continua = False
			i += 1
		return len(set(L))
	def __eq__(self, other):
		if self is not None and other is not None and self.__id==other.__id and self.__color==other.__color:
			return self._hijo_izq==other._hijo_izq and self._hijo_der==other._hijo_der
		elif self is None and other is None:
			return True
		else:
			return False
	def postOrden(self):
		str0 = ''
		if self.existeHijoIzq():
			str0 += self.getHijoIzq().postOrden()
		if self.existeHijoDer():
			str0 += self.getHijoDer().postOrden()
		return str0+self.getId().__str__()+", "
